﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCharacter : MonoBehaviour
{
    public Vector3 basePosition;
    public Animator animator;
    public ParticleSystem particularHitted;
    public Player player;

    int currentHealth;

    public HealthBar healthBar;

    private float dazedTime;
    public float startDazedTime;
    private void Start()
    {
        animator.SetBool("isDead", false);
        particularHitted.Stop();
        basePosition = transform.position;
        particularHitted.startSize = 10;
        currentHealth = player.health;
        healthBar.SetMaxHealth(player.health);
        animator.ResetTrigger("Attack");
        player.isDead = false;
        this.enabled = true;
        GetComponent<Collider2D>().enabled = true;
    }

    public void TakeDamage(int damage)
    {
        //dazedTime = startDazedTime;
        particularHitted.Play();
        currentHealth -= damage;
        healthBar.SetHealt(currentHealth);
        //nextAttackTime = Time.time + 1 / monster.attackSpeed;

        animator.SetTrigger("Hurt");

        if (currentHealth <= 0)
        {
            particularHitted.startSize = 20;

            /*
        //    particularHitted.Stop();
        //            Invoke("StopParticular", 0.3f);

        //*/
            Die();
        }
    }

    void Die()
    {
        Debug.Log("Died");
        animator.SetBool("isDead", true);
        player.isDead = true;
        GetComponent<Collider2D>().enabled = false;
        this.enabled = false;
        Invoke("RemoveCharacter", 5f);
    }
    void RemoveCharacter()
    {
        //Destroy(gameObject);
        GetComponent<Renderer>().enabled = false;
        Reborn();
    }

    void Reborn()
    {
        //Instantiate(gameObject, basePosition, Quaternion.identity);
        //Invoke("RemoveCharacter", 1f);
        transform.position = basePosition;
        GetComponent<Renderer>().enabled = true;
        Start();

    }
}
