﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public Player player;
    public Rigidbody2D rb;
    public Animator animator;
    public Transform attackPose;
    Vector2 movement;
    public GameObject target;
    public Transform wrapper;
    // Update is called once per frame
    void Update()
    {
        //Input
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        //animator.SetFloat("Horizontal", movement.x);

        //more optimized way to sqr magnitude for better performence
        animator.SetFloat("Speed", movement.sqrMagnitude);

        //if(movement.sqrMagnitude > 0.1)
        //{
        //    //For determine direction of face on stop
        //    animator.SetFloat("LastHorizontal", movement.x);
        //}

        Vector3 playerPoseScale = transform.localScale;
        Vector3 targetPoseScale = target.transform.localScale;
        //if (movement.x < 0)
        //{
        //    playerPoseScale.x = -10;
        //}
        //else if (movement.x > 0)
        //{

        //    playerPoseScale.x = 10;
        //}
        if (target.transform.rotation.z > 0.7f || target.transform.rotation.z < -0.7)
        {
            playerPoseScale.x = -10;
            targetPoseScale.x = -0.03f;
        }
        else
        {
            playerPoseScale.x = 10;
            targetPoseScale.x = 0.03f;
        }

        transform.localScale = playerPoseScale;
        target.transform.localScale = targetPoseScale;

    }

    void FixedUpdate()
    {
        //Movement
        //rb.position for the current position and movement for the direction
        //fixed delta time is for the constant movement
        if (!player.isDead)
        {
            rb.MovePosition(rb.position + movement * player.speed * Time.fixedDeltaTime);
        }
    }
}
