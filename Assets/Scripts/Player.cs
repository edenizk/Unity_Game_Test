﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public new string name = "Player";
    public int health = 100;
    public int damage = 1;
    public float attackRange = 1.5f;
    public float speed = 15f;
    public float attackSpeed = 3f;
    public float rangeAttackSpeed = 3f;
    public float rangeAttackDamage = 10f;
    public float rangeAttackLifeTime = 1.5f;
    public bool isDead = false;
}
